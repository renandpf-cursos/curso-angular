export interface DaoInterface<T> {
  tableName: string;

  insert(object: T): boolean;
  udate(object: T): boolean;
  delete(id: number): T;
  find(id: number): T;
  findAll(): [T];//O retorno tb pode ser Array<T>
}
