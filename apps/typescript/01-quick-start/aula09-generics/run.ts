import {DaoInterface} from './dao.interface';
import {Dao} from './dao';
import {Animal} from './../aula07-classes/animal';


let dao: DaoInterface<Animal> = new Dao<Animal>();

dao.insert(new Animal('Novo Animal'));
