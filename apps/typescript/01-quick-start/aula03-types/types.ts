//Parametro com tipo
function logTyped(mensage: string){
  console.log(mensage);
}
logTyped('TESTE');
//logTyped(1);//Ocorre erro

//Parametro sem tipo
function logNoTyped(mensage){
  console.log(mensage);
}
logNoTyped(15);
logNoTyped("AA");

function logAnyTyped(mensage: any){
  console.log(mensage);
}
logAnyTyped(10);
logAnyTyped("ASAS")


let teste: Array<string> = ['ola','teste'];
let isActive: true;
