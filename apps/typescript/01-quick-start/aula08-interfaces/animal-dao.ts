import {DaoInterface} from './dao.interface';
import {Animal} from './../aula07-classes/animal';

export class AnimalDao implements DaoInterface{

  tableName: string = '';

  insert(animal: Animal): boolean{
    console.log('Inserting ',animal.getNome());
    return false;
  };

  udate(animal: Animal): boolean {
    return false;
  };


  delete(idAnimal: number): Animal{
      return null;
  };

  find(idAnimal: number): Animal{
    return null;
  };

  findAll(): [Animal]{
    return [new Animal('Rex')];
  };

}
