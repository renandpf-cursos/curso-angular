import { Component, OnInit } from '@angular/core';
import { Contato } from '../models/contato.model';
import { ContatoService } from './contato.service';
import {DialogService} from '../dialog.service';

@Component({
  moduleId: module.id,
  selector: 'contatos-lista',
  templateUrl: 'contatos-lista.component.html'
})
export class ContatosListaComponent implements OnInit{//Os componente possuem ciclo de vida
    private contatos: Array<Contato> = [];

    private mensagem:{};
    private classesCss:{};
    private currentTimeout:any;

    constructor(
        private contatoService: ContatoService,
        private dialogService: DialogService
    ){}

    //Outra forma de fazer um construtor usando injeção de dependencias.
    //constructor(private contatoService: ContatoService){}

    //implements
    ngOnInit(): void {
      //this.contatoService.getContatos().
      this.contatoService.findAll().
        then(contatos => {
          this.contatos = contatos;
        }).
        catch(err => {
          this.mostrarMensagem({
            tipo: 'danger',
            texto: 'Erro ao buscar contatos.'
          });
        });
    }

    onDelete(contato:Contato):void{
      this.dialogService.confirm("Deseja deletar o contato?")
        .then((canDelete:boolean) => {
          if(canDelete){
              this.contatoService
                .delete(contato)
                .then(() => {
                    this.contatos = this.contatos.filter(c => c.id !== contato.id);
                    this.mostrarMensagem({
                      tipo: 'success',
                      texto: 'Contato Deletado!'
                    });
                  })
                .catch(err => {
                  this.mostrarMensagem({
                    tipo: 'danger',
                    texto: 'Ocorreu um erro ao deletar'
                  });
                });
          }
        });
    }

    private mostrarMensagem(mensagem:{tipo:string, texto:string}):void{
      //class="alert alert-success"
      this.mensagem = mensagem;
      this.montarClasses(mensagem.tipo);

      if(mensagem.tipo !== 'danger'){

        //Necessário pois sem isso, ocorre comportamento ruim se user ficar clicando em deletar rapido demais
        if(this.currentTimeout){
          clearTimeout(this.currentTimeout);
        }

        this.currentTimeout = setTimeout(()=>{//Limpa a mensagem após 3 segundos
          this.mensagem = undefined;
        }, 3000);
      }
    }

    private montarClasses(tipo:string):void{
      /*
        'alert': true,
        'alert-success': true,
        'alert-danger':false
        ...
      */
      this.classesCss ={
        'alert':true
      };
      this.classesCss['alert-'+tipo]=true;
    }


}
