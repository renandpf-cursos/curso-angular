import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {ContatoDetalheComponent} from './contato-detalhe.component';
import {ContatosListaComponent} from './contatos-lista.component';
import {ContatoRoutingModule} from './contato-routing.module';
import {ContatoBuscaComponent} from './contato-busca.component';

import { ContatoService } from './contato.service';

@NgModule({
  imports:[
    CommonModule,
    ContatoRoutingModule,
    FormsModule
  ],
  declarations: [
    ContatoDetalheComponent,
    ContatosListaComponent,
    ContatoBuscaComponent
  ],
  exports:[
    ContatosListaComponent,
    ContatoBuscaComponent
  ],
  providers: [
    /*
        Neste ponto, a cada chamada dos componentes deste modulo (declarado em "declarations") ,
        é utilizada a mesma instância (na injeção).

        Caso queira uma nova instância (por component), crie diretamente nele.

        Caso queira compartilhar entre modulos, ela deve ser disponibilizada
        mas em camadas superiores da app (app.component ou app.module)
    */
    ContatoService
  ]
})
export class ContatosModule {}
