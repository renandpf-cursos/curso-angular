import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';

import {Contato} from '../models/contato.model';
import {ContatoService} from './contato.service';

@Component({
  moduleId: module.id,
  selector: 'contato-detalhe',
  templateUrl: 'contato-detalhe.component.html'
  /*styles: [`
        .ng-valid[required]{
          border: 1px solid green;
        }
        .ng-invalid:not(form){
          border: 1px solid red;
        }
    `]/*,//Serve para usar um arquivo css existe
    styleUrls: [
      './arquivo.css'
    ]*/
})
export class ContatoDetalheComponent implements OnInit{

  private contatoService:ContatoService;
  private contato:Contato;
  private isNew:boolean;
  private route:ActivatedRoute;
  private location:Location;

  constructor(contatoService:ContatoService,
              route:ActivatedRoute,
              location:Location){
      this.contatoService = contatoService;
      this.route = route;
      this.location = location;

      this.contato = new Contato();
      this.isNew = true;
  }

  public ngOnInit():void{
    this.route.params.forEach(params => {
      let contatoId = +params["id"];//O "+" serve para converter para o tipo correto.

      //Só chamamos o serviço se o id for valido
      if(contatoId){
        this.isNew = false;
        this.contatoService.find(contatoId).
        then(contato => this.contato = contato);
      }
    });
  }

  onSubmit():void{
    let promise = null;

    if(this.isNew){
      promise = this.contatoService.create(this.contato);
    }else{
      promise = this.contatoService.update(this.contato);
    }

    //Utiliza o voltar do navegador
    promise.then(contato => this.goBack());
  }

  goBack():void{
    this.location.back();
  }


}
