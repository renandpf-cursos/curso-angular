import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {Contato} from '../models/contato.model';
import {ServiceInterface} from '../interfaces/service.interface';
import {CONTATOS} from '../models/contatos.mock';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ContatoService implements ServiceInterface<Contato>{


  //"app" é o nome do diretorio da aplicação.
  //"contatos" é o nome da varivel em "InMemoryDataService.createDb"
  private contatosUrl:string = "app/contatos";

  private headers: Headers = new Headers({'Content-Type':'application/json'});

  constructor(
    private http:Http
  ){};

  /*  Um "Promise", é uma promessa de entrega.
      Ele executa uma função de callBack logo após os dados estarem prontos.
  */
  public findAll(): Promise<Array<Contato>>{
    return this.http.get(this.contatosUrl)
      .toPromise()//O get do Angular é simples, e sozinho não faz o "toPromise". Por isso foi necessário importar o operador do rxjs
      .then(response => response.json().data as Array<Contato>)
      .catch(err => this.handleError(err));
  }


  public find(id:number): Promise<Contato>{
    const url = `${this.contatosUrl}/${id}`; // app/contatos/:id
    return this.http
      .get(url)
      .toPromise()
      .then(response => response.json().data as Contato);
  }

  public create(contato:Contato):Promise<Contato>{
    return this.http
      .post(this.contatosUrl, JSON.stringify(contato), {headers:this.headers})
      .toPromise()
      .then(response => response.json().data as Contato)
      .catch(this.handleError);
  }

  public update(contato:Contato):Promise<Contato>{
    const url = `${this.contatosUrl}/${contato.id}`; // app/contatos/:id
    return this.http
      .put(url, JSON.stringify(contato), {headers:this.headers})
      .toPromise()
      .then(() => contato as Contato)
      .catch(this.handleError);
  }

  public delete(contato:Contato): Promise<Contato>{
    const url = `${this.contatosUrl}/${contato.id}`; // app/contatos/:id
    return this.http
      .delete(url)
      .toPromise()
      .then(() => contato as Contato);
  }

  //Este método deve ser generico e lança mensagem para ser tratada.
  //O "Promise.reject" faz com o chamador caia também no catch
  private handleError(err: any):Promise<any>{
    return Promise.reject(err.message || err);
  }

  /*  No método abaixo é demostrado a adição de timeouts.
      Também é demostrada o encadeamento de Promisses.
        O encadeamento de prommises pode ser utilizado para encadear chamadas assincronas...
        Por exemplo:  No primeiro them, pode ser chamada uma função de buscar usuário
                      No segundo, pode chamar outra função assincrona para ver se o usuário tem acesso (já que ele pode ser enviado pro próximo Promise)
                      Na quarta, pode fazer alguma lógica.

        Importante ressaltar que enquanto um "then" não terminar o outro não começa
  */
  //Simula uma chamada lenta
  public getContatosSlowly(): Promise<Array<Contato>>{

    return new Promise((resolve, reject) => {

      return new Promise((resolve1,reject1) => {
        console.log("Chamada do servidor (A) iniciada...");
        console.log("Aguardando servidor (A)...");
        setTimeout(() => {
          resolve();
          console.log("Chamada do servidor (A) encerrada");
        }, 5000);//Adiciona um timeout, para depois executar a função "then"...
      });


    }).then(() => {
      console.log("Segundo Promisse (não usei o retorno anterior)");

      //Pode retornar qualquer coisa.
      //E o retorno é recebido como parâmetro no próximo encadeamento
      return "Teste de Retorno";

    }).then((parametroAnterior) => {
      console.log("Terceiro Promisse: "+parametroAnterior);

      //Em qualquer then pode fazer chamadas assincronas pro servidor...

      return new Promise((resolve2, reject2) => {
        console.log("Chamada no servidor (B) INICIADA...");

        console.log("Aguardando servidor (B) ...");
        setTimeout(() => {
            resolve2();//Se não executar essa linha, o proximo "then" não é chamado
            console.log("Chamada no servidor (B) TERMINADA...");
        },5000);
      });

    })
    .then (() => this.findAll());
  }

  /*
    Observable:
    É um fluxo de eventos que podemos processar operadores do tipo "arrayLike" (pesquisar sobre) .
    ArrayLike, é um objeto javaScript que se parece com um array, mas não é um array.

    O core do angular2 tem um suporte basico para trabalhar com Observable.
    Então, para se trabalhar a fundo, devemos extender da "rxjs" (pesquisar sobre) .

    Pesquisar sobre "Programação Reativa".

    A maior parte dos métodos dessa classe utiliza promisses. Promisse é mais facil e de maior utiliade,
    quando a url é chamada uma unica vez e o resultado é "unico" (sempre uma lista de contatos chamada após um evento simples)

    No caso de uma chamada de busca, onde o usuário começa pesquisando com um nome e na mesma hora muda o nome,
    é recomendado usar Observable.

    *** Então, existe situações mais específicas de usar Promisses. E existem situações mais específicas de usar Observable.

    Mas nada impede de usar somente Observable (nesta classe por exemplo.)
    Mas fazer a funcionalidade de busca, torna-se dificil de usar Promise.

  */
  search(term: string ): Observable<Array<Contato>> {
    return this
            .http
            .get(`${this.contatosUrl}/?nome=${term}`)
            .map((res) => res.json().data as Array<Contato>);
  }


}
