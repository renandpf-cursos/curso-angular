import { Component, EventEmitter, OnInit, OnChanges, Input, Output, SimpleChange, SimpleChanges } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {ContatoService} from './contato.service';
import {Contato} from '../models/contato.model';

@Component({
  moduleId: module.id,
  selector: 'contato-busca',
  templateUrl: 'contato-busca.component.html',
  styles: [`
    .cursor-pointer:hover{
      cursor: pointer;
    }
  `]/*,//Exemplo para não usar o "@Input" no parametro. O recomendado é usar a anotação.
    inputs: [
      'busca:mySearch' //propertyName:alias
    ]*/
})
export class ContatoBuscaComponent implements OnInit, OnChanges {


  contatos:Observable<Array<Contato>>;


  /*
  Expõe a variavel para receber dados de entrada.
  O nome dentro do "()" (se for informado), é o nome da propriedade a ser utilizada na tag
  Outra maneira de fazer o input é adicionar na anotação "@Component" da classe do component (vide esta classe) .
  Neste caso não é necessário haver o "@Input"

  Apesar da informação externa chegar nele, não significa que uma ação irá ocorrer.
  Neste caso, foi implementado o ciclo de vida "OnChanges" para que seja verificado alterações neste campo.

  Para funcionar o twoWay databind com Input e OutPut a proporiedades (definidas na classe) devem seguir
      o padrão:
        Input: x
        Output: xChange

        "termo" é a variável definida no input "qualquerNome" (neste arquivo)
        "busca" refere-se as propriedades "Input" (busca) e "Output" (buscaChange) definidas na classe

  */
  @Input()
  busca:String;

  /*
  A saida está sendo um emissor de eventos (TODO - entender direito)

  */
  @Output()
  buscaChange:EventEmitter<string> = new EventEmitter<string>();

  /*
    "Subject" é basicamente um produtor de fluxo de eventos observaveis.
    Em suma, trata-se de outro Observable
    Cada vez que o usuário digitar no campo, será salvo neste item. E ele, por sua vez,
    tem a capacidade de gerenciar a ordem e fluxo de cada evento digitado.
    Analogicamente, pode-se entender como se ele fosse um array de eventos.
    Na pratica, ele gerência todo o fluxo de entradas digitadas pelo usuário

  */
  private termosBusca:Subject<string> = new Subject<string>();


  constructor(
    private contatoService:ContatoService,
    private router:Router

  ) {}

  ngOnInit():void {
    this.contatos = this.termosBusca
      .debounceTime(600) //Isso faz com que seja aguardado x milessegundos para envio no servidor. Serve para diminuir a qnt. de requisições pro server
      .distinctUntilChanged()//Ignore se o proximo termo de busca foi igual ao anterior (mas não guarda hitorico dos termos)
      .switchMap(//sempre retorna a ultima requisição enviada pro server
        term => term ? this.contatoService.search(term) : Observable.of<Array<Contato>>())
      .catch(err => {
        console.log(err);
        return Observable.of<Array<Contato>>([]);
      });
  }

  /*
    Este método (do clico de vida "OnChanges") é chamado sempre que alguma alteração ocorre nos
    parametros que estão anotados com "@Input"

    O parametro "SimpleChanges" contem uma coleção de "SimpleChange" que possui as alterações de "antes" e "depois" do campo.

  */
  ngOnChanges(changes: SimpleChanges):void{
    let buscaSimpleChange:SimpleChange = changes["busca"];
    this.search(buscaSimpleChange.currentValue);
  }

  search(term:string):void{
    this.termosBusca.next(term);
    this.buscaChange.emit(term);
  }

  verDetalhe(contato:Contato):void{
    let link = ['contato/save',contato.id];
    this.router.navigate(link);
    this.buscaChange.emit("");
  }


}
