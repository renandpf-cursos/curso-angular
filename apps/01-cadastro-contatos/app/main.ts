
//platformBrowserDynamic não deve ser usado em PRD, pois ele compila a aplicação durante a execução.
//Em PRD o certo é usar platform-browser (pré compila o código)
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import {AppModule} from './app.module';

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);
