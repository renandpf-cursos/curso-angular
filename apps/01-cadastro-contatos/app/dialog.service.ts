import {Injectable} from '@angular/core';

@Injectable()
export class DialogService {

  confirm(message?:string){//? significa que é opcional
    return new Promise(resolve => {
        return resolve(window.confirm(message || 'Confirmar?'));
    });
  }


}
