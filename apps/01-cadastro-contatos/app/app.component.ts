import {Component} from '@angular/core';

//O template pode ser uma url ou "inLine", conforme esse exemplo
@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl:'app.component.html'
})
export class AppComponent{

}
