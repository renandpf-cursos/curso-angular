import {InMemoryDbService} from 'angular-in-memory-web-api';
import {Contato} from './models/contato.model';

export class InMemoryDataService implements InMemoryDbService{

  private CONTATOS: Array<Contato> = [
    {id:1, nome: 'Fulano 10', email: 'fulano1@gmail.com', telefone: '(01) 0000-0000'},
    {id:2, nome: 'Fulano 22', email: 'fulano2@gmail.com', telefone: '(02) 0000-0000'},
    {id:3, nome: 'Fulano 33', email: 'fulano3@gmail.com', telefone: '(03) 0000-0000'}
  ];

  createDb():{}{
    //Cada nome de variável torna-se parte da URL
    let contatos: Array<Contato> = this.CONTATOS;
      return {contatos};
  }

}
