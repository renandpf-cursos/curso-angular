import './util/rxjs-extensions'
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';//Indica ao angular para acessar uma API simulado (local)
import {InMemoryDataService} from './in-memory-data.service';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRountingModule} from './app-rounting.module';
import {ContatosModule} from './contatos/contatos.module';
import {DialogService} from './dialog.service';

//Decormaos as classes para que o angular possa utiliza-as
@NgModule({
  imports:[//É o ponto de entrada. No caso o browser
    AppRountingModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    ContatosModule
  ],
  declarations: [AppComponent],//Todos os componentes que serão usados no modulo devem ser declarados aqui
  providers: [DialogService],
  bootstrap: [AppComponent]//
})
export class AppModule {}
