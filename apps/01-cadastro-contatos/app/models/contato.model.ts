export class Contato {

  public id: number;
  public nome: string;
  public email: string;
  public telefone: string;

  //Os atributos das classes podem ser criadas assim (no construtor) ou direto na classe (forma convencional) .
  /*constructor(
    public id: number,
    public nome: string,
    public email: string,
    public telefone: string
  ){}*/

}
